#!/usr/bin/env python3
from datetime import datetime, timezone
from gutenberg_schema import Agent, Ebook, Base
from sqlalchemy.orm import sessionmaker
from rdflib import URIRef as URI
import argparse
import maya
import os
import os.path
import rdflib as rdf
import re
import sqlalchemy as sqa


def book_number(filename):
    '''Return the ebook id embedded in the filename'''
    return int(re.match(r'pg(\d+)\.rdf', filename).group(1))

def book_url(book_num):
    return URI('http://www.gutenberg.org/ebooks/{}'.format(book_num))

def agent_number(url):
    '''Return the agent number embedded in the url'''
    return int(re.match(r'http://www.gutenberg.org/2009/agents/(\d+)',
        str(url)).group(1))

def agent_url(agent_number):
    return URI('http://www.gutenberg.org/2009/agents/{}'.format(agent_number))

def first(l):
    '''Return the first element if it exists or None otherwise'''
    try:
        return next(iter(l))
    except StopIteration:
        return None

def val_or_default(val, default):
    return default if val is None else val

def apply_non_none(val, to_apply):
    'Return to_apply(val) if val is not None or None otherwise'
    return None if val is None else to_apply(val)

def parse_date(val):
    'Parse the date into a datetime'
    if re.match('\d+', str(val)):
        return datetime(year=int(val), month=1, day=1, tzinfo=timezone.utc)
    else:
        return maya.parse(val).datetime()

def agent_record(agent_num, sqa_session, rdf_graph):
    '''
    Return an Agent object filled out from the sqlalchemy session and
    rdf_graph
    '''
    name_pred =  URI('http://www.gutenberg.org/2009/pgterms/name')
    birth_pred = URI('http://www.gutenberg.org/2009/pgterms/birthdate')
    death_pred = URI('http://www.gutenberg.org/2009/pgterms/deathdate')

    g = rdf_graph
    c = val_or_default(sqa_session.query(Agent).filter_by(id=agent_num).first(),
                       Agent(id=agent_num))
    c_url = agent_url(agent_num)
    c.name = apply_non_none(first(g.objects(c_url, name_pred)), str)
    if c.name is None:
        sys.err.write('ERROR: {} had no name'.format(agent_url(c.id)))
        return
    c.birthdate = apply_non_none(
        val_or_default(first(g.objects(c_url, birth_pred)),
                       c.birthdate), parse_date)
    c.deathdate = apply_non_none(
        val_or_default(first(g.objects(c_url, death_pred)),
                       c.deathdate), parse_date)

    return c

def process_file(dir_, filename, session_class):
    '''Copy file's contents to database session'''
    creator_pred = URI('http://purl.org/dc/terms/creator')
    title_pred =   URI('http://purl.org/dc/terms/title')
    lang_pred =    URI('http://purl.org/dc/terms/language')
    translator_pred = URI('http://id.loc.gov/vocabulary/relators/trl')
    value_pred = URI('http://www.w3.org/1999/02/22-rdf-syntax-ns#value')

    session = session_class()
    g = rdf.Graph()
    g.parse(os.path.join(dir_, filename))

    num = book_number(filename)
    b_url = book_url(num)
    book = val_or_default(session.query(Ebook).filter_by(id=num).first(),
        Ebook(id=num))
    agents=dict()
    book.language = first(g.objects(b_url, lang_pred))
    book.title = first(g.objects(b_url, title_pred))
    if isinstance(book.language, rdf.BNode):  # repl with transitive_closure?
        book.language = first(g.objects(book.language, value_pred))
    book.title = first(g.objects(b_url, title_pred))
    session.add(book)

    creators = [agent_number(o) for s, p, o in
        g.triples((b_url, creator_pred, None))]
    for agent_num in creators:
        c = agent_record(agent_num, session, g)
        c.ebooks_created.add(book)
        session.add(c)

    translators = [agent_number(o) for s, p, o in
        g.triples((b_url, translator_pred, None))]
    for agent_num in translators:
        t = agent_record(agent_num, session, g)
        t.ebooks_translated.add(book)
        session.add(t)

    session.commit()


def process_files(root_path, session_class):
    '''
    Find all the pg*.rdf files in the tree rooted at root_path
    and add their contents to the database from session
    '''
    for dir_, subdirs, files in os.walk(root_path):
        for filename in files:
           if re.match(r"pg\d+\.rdf", filename):
               process_file(dir_, filename, session_class)


argparser = argparse.ArgumentParser(description="Add Project Gutenberg metadata to database")
argparser.add_argument('target_file', type=str, help='path to sqlite file where metadata will be placed')
argparser.add_argument('metadata_base', help='path to root of Project Gutenberg metadata (from unzipping machine readable catalog). Should have directories with files named pg*.rtf')


args = argparser.parse_args()

engine = sqa.create_engine('sqlite:///' + args.target_file)
Base.metadata.create_all(engine)
Session = sessionmaker(bind=engine)
process_files(args.metadata_base, Session)
