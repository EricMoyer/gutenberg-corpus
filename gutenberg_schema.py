from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, sessionmaker
import sqlalchemy as sqa


Base = declarative_base()


# Many-to-many association table between ebooks and creators
ebooks_creators = sqa.Table('ebooks_creators', Base.metadata,
    sqa.Column('ebook_id', sqa.ForeignKey('ebooks.id')),
    sqa.Column('creator_id', sqa.ForeignKey('agents.id'))
)


# Many-to-many association table between ebooks and translators
ebooks_translators = sqa.Table('ebooks_translators', Base.metadata,
    sqa.Column('ebook_id', sqa.ForeignKey('ebooks.id')),
    sqa.Column('translator_id', sqa.ForeignKey('agents.id'))
)


class Agent(Base):
    __tablename__ = 'agents'
    # Number parsed from the tail of the agent url
    # e.g. http://www.gutenberg.org/2009/agents/40396 gets 40396
    id = sqa.Column(sqa.Integer, primary_key=True, autoincrement=False,
        nullable=False)
    # The agent's name ... shouldn't ever be missing because they have
    # agents like "Various" and "Anonymous"
    name = sqa.Column(sqa.String, nullable=False)
    # Usually just a year, frequently unavailable
    birthdate = sqa.Column(sqa.Date)
    # Usually just a year, frequently unavailable
    deathdate = sqa.Column(sqa.Date)
    ebooks_created = relationship('Ebook', secondary=ebooks_creators,
                                  back_populates='creators',
                                  collection_class=set)
    ebooks_translated = relationship('Ebook', secondary=ebooks_translators,
                                     back_populates='translators',
                                     collection_class=set)


class Ebook(Base):
    __tablename__ = 'ebooks'
    id = sqa.Column(sqa.Integer, primary_key=True, autoincrement=False,
        nullable=False)
    # Title of the ebook - Null = unavailable
    title = sqa.Column(sqa.String, nullable=True)
    # Language of the ebook - Null = unavailable
    language = sqa.Column(sqa.String, nullable=True)
    creators = relationship('Agent', secondary=ebooks_creators,
                            back_populates='ebooks_created',
                            collection_class=set)
    translators = relationship('Agent', secondary=ebooks_translators,
                               back_populates='ebooks_translated',
                               collection_class=set)
    sentences = relationship('Sentence', back_populates='book')


class Sentence(Base):
    __tablename__='sentences'
    # Number of sentence in ebook - 0 based
    sentence_idx = sqa.Column(sqa.Integer, primary_key=True,
                                 autoincrement=False, nullable=False)
    # Id of book to which sentence belongs
    ebook_id = sqa.Column(sqa.ForeignKey('ebooks.id'), primary_key=True,
                          autoincrement=False, nullable=False)

    # Number of containing paragraph in ebook - 0 based
    paragraph_idx = sqa.Column(sqa.Integer, nullable=False)

    # Text of the sentence - not nullable because it wouldn't be
    # recorded if the text were not available
    text = sqa.Column(sqa.String, nullable=False)

    # The book from which this sentence comes
    book = relationship('Ebook', back_populates='sentences')
