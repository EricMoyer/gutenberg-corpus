#!/usr/bin/env python3
from gutenberg_schema import Ebook, Base, Sentence
from nltk.tokenize.punkt import PunktTrainer, PunktSentenceTokenizer
from sqlalchemy.orm import sessionmaker
import argparse
import logging
import os
import os.path
import random
import re
import sqlalchemy as sqa
import sys


def paragraphs(text):
    '''Generate the paragraphs (separated by blank line) in text'''
    lines = text.strip().splitlines()
    if not lines:
        return
    cur = []
    for line in lines:
        if line == '':
            if len(cur) > 0:
                yield ' '.join(cur)
            cur = []
        else:
            cur.append(line.strip())
    yield ' '.join(cur)


def ebook_path_stem(ebook_id, base_path):
    '''Return the path to the ebook without file format specific extensions'''
    str_id = str(ebook_id)
    prefixes=[c for i,c in enumerate(str_id) if i+1 != len(str_id)]
    if len(str_id) == 1:
        prefixes = ['0'] + prefixes
    return os.path.join(base_path, *prefixes, str_id, str_id)


def ebook_filename_and_codec(stem):
    '''Returns the first (filename, codec) pair for a given stem that exists
    If none exist, returns None for both

    The stem is a path into the Gutenberg ebook tree without
    type-defining extentions
    '''
    suffix_codec = [('.txt.utf-8', 'utf_8'), ('-8.txt', 'latin_1'),
                    ('.txt', 'latin_1')]
    for suffix, codec in suffix_codec:
        filename = stem + suffix
        if os.path.isfile(filename):
            return filename, codec
    return None, None


def ebook_raw_text(ebook_id, base_path):
    '''Return the text of the ebook including boilerplate. None if the
    text was not found'''
    filename, codec = ebook_filename_and_codec(
        ebook_path_stem(ebook_id, base_path))
    if codec is None:
        return None
    else:
        return open(filename, 'rt', encoding=codec).read()


def ebook_paragraphs(ebook_id, base_path):
    '''Return the text of the ebook broken into paragraphs or None if the
    text and boilerplate could not be separated or if the ebook text
    could not be found'''
    text = ebook_raw_text(ebook_id, base_path)
    if text is None:
        return None

    flags = re.IGNORECASE | re.DOTALL
    start = re.compile(r'\W*START\s+.*PROJECT GUTENBERG EBOOK', flags)
    end = re.compile(r'\W*END\s+.*PROJECT GUTENBERG EBOOK', flags)
    produced_by = re.compile(r'.*(?:(?:produced|prepared) by'
                             r'|proofreaders|project gutenberg|\bebook\b'
                             r'|\betext\b)',
                             flags)

    trimmed = []  # Paragraphs without boilerplate
    seen_start_but_no_end = False
    for p in paragraphs(text):
        if start.match(p):
            seen_start_but_no_end = True
            continue  # skip the START paragraph
        elif end.match(p):
            seen_start_but_no_end = False
            continue  # skip the END paragraph
        elif seen_start_but_no_end:
            if not (len(trimmed) == 0 and produced_by.match(p)):
                trimmed.append(p)
    return trimmed if len(trimmed) > 0 else None

# Allow excluding books that don't use the period correctly from the
# training set. Values are regexes to detect one instance of proper
# period use in the text for a given language.
proper_period_use_regex = {
    'en': re.compile(r'\b(?:Mr|Dr|Mrs|Miss|St|Rev|Fr|Ms)\.'),
    'es': re.compile(r'\b(?:Sr|Sra|Dr|Srta)\.')
}
improper_period_use_regex = {
    'en': re.compile(r'\b(?:Mr|Dr|Mrs|Miss|St|Rev|Fr|Ms)\b[^.]'),
    'es': re.compile(r'\b(?:Sr|Sra|Dr|Srta)\b[^.]')
}
never_matches = re.compile("^a\bc")
always_matches = re.compile(".*")

def train_sentence_tokenizer(ebook_ids, lang, base_path):
    '''Train a sentence tokenizer on all ebooks (data lies off base_path)'''
    trainer = PunktTrainer()
    sample_size = min(len(ebook_ids), 128)
    sampled_ids = random.sample(ebook_ids, sample_size)
    proper = proper_period_use_regex.get(str(lang), always_matches)
    improper = improper_period_use_regex.get(str(lang), never_matches)
    for ebook_id in sampled_ids:
        logging.info('Training sentence tokenizer on ebook {}'.format(ebook_id))
        text = ebook_paragraphs(ebook_id, base_path)
        if text is not None:
            joined = '\n\n'.join(text)
            if proper.search(joined) and not improper.search(joined):
                trainer.train(joined, finalize=False, verbose=False)
            else:
                logging.info('Not training on ebook {} because it might not '
                             'use abbreviation punctuation consistently'.format(
                                 ebook_id))
        else:
            logging.info('Skipping ebook {} because could not remove '
                         'boilerplate'.format(ebook_id))
    trainer.finalize_training(verbose=False)
    return PunktSentenceTokenizer(trainer.get_params(), verbose=False)


def add_lang(lang, base_path, Session):
    '''Adds all works with a given language located at base_path to a
    session created from Session'''
    session = Session()
    ebook_ids = [i[0] for i in session.query(Ebook.id).filter(
        Ebook.language==lang).all()]
    session.commit()
    logging.debug('Adding language {}: {} books'.format(lang, len(ebook_ids)))

    splitter = train_sentence_tokenizer(ebook_ids, lang, base_path)
    for ebook_id in ebook_ids:
        if session.query(Sentence.ebook_id).filter(
                Sentence.ebook_id == ebook_id).first():
            logging.info('Skipping ebook {} because it already has '
                          'sentences'.format(ebook_id))
            continue

        logging.debug('Adding ebook {}'.format(ebook_id))

        paras = ebook_paragraphs(ebook_id, base_path)
        if paras is None:
            logging.warning('Could not remove boilerplate for ebook '
                            '{}'.format(ebook_id))
            continue
        sentence_idx = -1
        for paragraph_idx, para in enumerate(paras):
            for sentence in splitter.tokenize(para):
                sentence_idx += 1
                s=Sentence(sentence_idx=sentence_idx,
                           ebook_id=ebook_id,
                           paragraph_idx=paragraph_idx,
                           text=sentence)
                session.add(s)
        logging.info('Added {} sentences from ebook {}'.format(sentence_idx+1,
                                                               ebook_id))
        session.commit()

# Putting top-level stuff in the if lets me import for smoke testing
if __name__ == '__main__':
    argparser = argparse.ArgumentParser(description="Fill in English "
                                        "sentences from Project Gutenberg")
    argparser.add_argument('target_file', type=str, help='path to sqlite '
                           'file where sentences will be placed - should '
                           'not be modified by other processes while this '
                           'process is running. Uses the metadata stored '
                           'here to select files, so metadata should already '
                           'be filled in. If any ebooks have sentences, they '
                           'will not be updated (allows stopping and '
                           'restarting the process later)')
    argparser.add_argument('gutenberg_base', help='path to root of Project '
                           'Gutenberg mirror. Should have directories 0, '
                           '1, ...')

    args = argparser.parse_args()

    logging.basicConfig(level=logging.INFO)
    engine = sqa.create_engine('sqlite:///' + args.target_file)
    Base.metadata.create_all(engine)
    Session = sessionmaker(bind=engine)
    for lang in Session().query(sqa.distinct(Ebook.language)).all():
        add_lang(lang[0], args.gutenberg_base, Session)
