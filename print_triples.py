#!/usr/bin/env python3
import rdflib as rdf
import sys

g = rdf.Graph()
g.parse(sys.argv[1])


for s, p, o in g:
    print("{}\t{}\t{}".format(s, p, o))

