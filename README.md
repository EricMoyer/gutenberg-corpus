Really quick install notes:
===========================

Main install steps
-------------------

You will need to sudo install python headers python-dev and
python3-dev for those to work.

    pip3 install sqlalchemy nltk rdflib maya flake8

Compatability and license
-------------------------

This was developed using the Windows Linux Subsystem on two boxes and a separate
install of Ubuntu Linux 16.10 so should work in both places.

License: Mozilla Public License 2.0 (I'll start with something relatively strong and possibly move to something weaker. I think GPL is stronger than I want at the moment.)
